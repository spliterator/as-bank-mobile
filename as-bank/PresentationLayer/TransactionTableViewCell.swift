//
//  TransactionTableViewCell.swift
//  as-bank
//
//  Created by Дмитрий Бутилов on 17.06.18.
//  Copyright © 2018 Дмитрий Бутилов. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var reciever: UILabel!
    @IBOutlet weak var value: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
