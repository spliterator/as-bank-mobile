//
//  TransactionController.swift
//  as-bank
//
//  Created by Дмитрий Бутилов on 17.06.18.
//  Copyright © 2018 Дмитрий Бутилов. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class TransactionController: UIBaseController {
    @IBOutlet weak var BillNameTextField: UITextField!
    
    @IBOutlet weak var TransactionValueTextField: UITextField!
    
    @IBAction func createTransactionButtonAction(_ sender: Any) {
        let isLogged = UserDefaults.standard.bool(forKey: UserKeys.userLogged)
        if (isLogged == false) {
            self.dismiss(animated: true, completion: nil)
        } else {
            if let billName :String = BillNameTextField.text {
                if let value :String = TransactionValueTextField.text {
                    if let billNumber :Int = Int(billName) {
                        if (1000000..<10000000).contains(billNumber) {
                            if let intValue :Int = Int(value) {
                                if (10..<100000).contains(intValue) {
                                    createTransaction(billName, value)
                                } else {
                                    displayMyAlertMessage(userMessage: "Неверное значение")
                                }
                            } else {
                                displayMyAlertMessage(userMessage: "Неверное значение")
                            }
                        } else {
                            displayMyAlertMessage(userMessage: "Неверный номер счета")
                        }
                    } else {
                    displayMyAlertMessage(userMessage: "Неверный номер счета")
                    }
                }
            }
        }
        BillNameTextField.text = ""
        TransactionValueTextField.text = ""
    }
    
    fileprivate func createTransaction(_ billName: String, _ value: String) {
        let token :String? = UserDefaults.standard.string(forKey: UserKeys.userToken)
        let login :String? = UserDefaults.standard.string(forKey: UserKeys.userLogin)
        
        let url: String = ServerSettings.address + "/transactions"
        let json: [String: Any] = ["user": ["login": login!],
                                   "destBill": ["name": billName],
                                   "value": value]
        let header: HTTPHeaders = ["Content-Type": "application/json", "charset": "utf-8", "token": token!]
        request(url, method: .post, parameters: json, encoding: JSONEncoding.default, headers: header).responseJSON { response in
            guard let statusCode = response.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            if (statusCode == 200) {
                self.displayMyAlertMessage(userMessage: "Перевод успешен");
            } else {
                self.displayMyAlertMessage(userMessage: "Не удалось перевести");
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
