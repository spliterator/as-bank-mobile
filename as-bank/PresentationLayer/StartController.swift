//
//  StartController.swift
//  as-bank
//
//  Created by Дмитрий Бутилов on 14.06.18.
//  Copyright © 2018 Дмитрий Бутилов. All rights reserved.
//

import Foundation
import UIKit

struct UserKeys {
    static let userToken = "tokenKey"
    static let userLogged = "isLoggedKey" // залогинился ли (получил ли токен)
    static let userLogin = "loginKey"
}

struct ServerSettings {
    static let address: String = "http://192.168.1.4:8086";
}

class StartController: UIViewController {
    
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ActivityIndicator.startAnimating()
        let isUserLoggedIn = UserDefaults.standard.bool(forKey: UserKeys.userLogged);
        if (isUserLoggedIn) {
            self.performSegue(withIdentifier: "TabsView", sender: self);
        }
        self.performSegue(withIdentifier: "LoginView", sender: self);
    }
}
