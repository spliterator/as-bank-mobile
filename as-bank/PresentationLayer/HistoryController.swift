
//
//  HistoryController.swift
//  as-bank
//
//  Created by Дмитрий Бутилов on 17.06.18.
//  Copyright © 2018 Дмитрий Бутилов. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HistoryController: UIBaseController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var TransactionTableView: UITableView!
    
    var transactions: JSON = []
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = TransactionTableView.dequeueReusableCell(withIdentifier: "cell") as! TransactionTableViewCell
        cell.cellView.layer.cornerRadius = cell.cellView.frame.height / 4.5
        let transaction: JSON = transactions[transactions.count - indexPath.row - 1]
        
        let timeImMillis: Int = transaction["time"].int!
        let date = Date(timeIntervalSince1970: (TimeInterval(timeImMillis / 1000)))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
        cell.time.text = "\(dateFormatter.string(from: date))"
        
        cell.reciever.text = transaction["destBill"].dictionary?["name"]?.string!
        cell.value.text = "\(transaction["value"].int!) ₽"
        
        return cell
    }
    
    fileprivate func requestTransactions(_ login: String?, _ token :String?) {
        let header: HTTPHeaders = ["Content-Type": "application/json", "charset": "utf-8", "token": token!]
        let url: String = ServerSettings.address + "/users/" + login! + "/transactions"
        request(url, method: .get, encoding: JSONEncoding.default, headers: header).responseJSON { responseJSON in
            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            if (statusCode == 200) {
                let swiftyJsonVar = JSON(responseJSON.result.value!)
                self.transactions = swiftyJsonVar
                self.TransactionTableView.reloadData()
            } else {
                self.displayMyAlertMessage(userMessage: "Ошибка");
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactions.count
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let isLogged = UserDefaults.standard.bool(forKey: UserKeys.userLogged)
        if (isLogged == false) {
            self.dismiss(animated: true, completion: nil)
        } else {
            let token :String? = UserDefaults.standard.string(forKey: UserKeys.userToken)
            let login :String? = UserDefaults.standard.string(forKey: UserKeys.userLogin)
            requestTransactions(login, token)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        TransactionTableView.delegate = self
        TransactionTableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
